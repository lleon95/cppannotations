With the introduction of the em(spaceship operator) (tt(<=>), cf. section
ref(SPACESHIP)) several em(comparison category classes) 
    hi(comparison classes) were added to the standard namespace.

Comparison classes are required when implementing the spaceship operator, and
to use them (or when declaring and implementing the spachip operator) the
tthi(compare) header file must be included.

The weak class types do not support emi(substitutability). Substitutability
means that if two objects tt(one) and tt(two) are equal (so: tt(one == two) is
tt(true)), then tt(fun(one) == fun(two)) is also tt(true). Here tt(fun) is any
function that only uses public const members of its argument that return
em(value types) (as compared to pointer types) which also support comparisons
(also called emi(comparison-salient state)).

The operators of comparison classes expect at least one argument of their own
class types. The other argument may either also be of their own class types or
it can be the value 0 (or any value that can be considered 0, like
tt(nullptr_t)).

There are five comparison classes, primarily used when implementing
spaceship operators:
    itemization(
    itt(weak_equality), used for classes that only support the tt(==) and
        tt(!=) operators, but not substitutability;
    itt(strong_equality), used for classes that only support the tt(==) and
        tt(!=) operators, as well as substitutability;
    itt(partial_ordering) used for classes that support all comparison
        operators, do not support substitutability, and can be used when
        comparing incomparable arguments (i.e., all comparison operators
        return false);
    itt(weak_ordering) used for classes that support all comparison
        operators and that do not support substitutability;
    itt(strong_ordering) used for classes that support all comparison
        operators as well as substitutability;
    )



