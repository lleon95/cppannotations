export module complete;

unsigned counter = 0;

double square(double value)
{
    return value * value;
}

class Class
{
    int d_value;

    public:
        Class(int value);
        int value() const;
};

Class::Class(int value)
:
    d_value(value)
{}

int Class::value() const
{
    return d_value;
}
