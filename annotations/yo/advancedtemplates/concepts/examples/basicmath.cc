template <typename LhsType, typename RhsType>
concept BasicMath =
    requires(LhsType lhs, RhsType rhs)
    {
        lhs + rhs;      // addition must be available
        lhs - rhs;      // subtraction must be available
        lhs * rhs;      // multiplication must be available
        lhs / rhs;      // division must be available
    };
